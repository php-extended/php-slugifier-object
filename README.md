# php-extended/php-slugifier-object

This library contains a slugifier that does ascii transliterations of unicode text.
This library is inspired by [martinml's implementation of transliterator](https://github.com/martinml/slugify)
and the [original python implementation](https://github.com/avian2/unidecode).

![coverage](https://gitlab.com/php-extended/php-slugifier-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-slugifier-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-slugifier-object ^8`


## Basic Usage

This library may be used the following way :

```php

use PhpExtended\Slugifier\SlugifierFactory;

$factory = new SlugifierFactory();
$slugifier = $factory->createSlugifier();
/* @var $slugifier \PhpExtended\Slugifier\SlugifierInterface */

```


There are different types of slugifiers available in this library :

- The `AsciiCutSlugifier` removes all characters from the input string
that are not digits or letters.
- The `LowercaseSlugifier` transforms the input string to lowercase
- The `UppercaseSlugifier` transforms the input string to uppercase
- The `NoopSlugifier` returns the input string untouched
- The `SlugifierChain` is made to chain multiple slugifiers to have a final
slugified string. Not that the order matters.
- The `AsciiTransliteratorSlugifier` transliterates the utf8 characters


## License

Original character transliteration tables:

Copyright 2001, Sean M. Burke <sburke@cpan.org>, all rights reserved.

Modifications on the tables and the code (/src) is:

MIT (See [license file](LICENSE)).
