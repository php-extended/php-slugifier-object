<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-slugifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Slugifier;

/**
 * TrimSlugifier class file.
 * 
 * This class removes the trailing elements of the string if it corresponds to
 * meaningless contents.
 * 
 * @author Anastaszor
 */
class TrimSlugifier implements SlugifierInterface
{
	
	/**
	 * The default options.
	 *
	 * @var SlugifierOptionsInterface
	 */
	protected SlugifierOptionsInterface $_defaultOptions;
	
	/**
	 * Builds the martinml bridge with default options for the slugifier.
	 *
	 * @param ?SlugifierOptionsInterface $default
	 */
	public function __construct(?SlugifierOptionsInterface $default = null)
	{
		if(null === $default)
		{
			$default = new SlugifierOptions();
		}
		
		$this->_defaultOptions = $default;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Slugifier\SlugifierInterface::isServiceable()
	 */
	public function isServiceable() : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Slugifier\SlugifierInterface::slugify()
	 */
	public function slugify(?string $string, ?SlugifierOptionsInterface $options = null) : string
	{
		$options = (null === $options ? $this->_defaultOptions : $this->_defaultOptions->mergeWith($options));
		$separator = $options->getSeparator();
		$string = (string) $string;
		
		if(0 === (int) \mb_strlen($separator))
		{
			return $string;
		}
		
		if(1 === (int) \mb_strlen($separator))
		{
			return (string) \trim($string, $separator);
		}
		
		// from here we can't use trim because separator may be multichar
		
		while((int) \mb_strlen($string) > (int) \mb_strlen($separator))
		{
			$tail = (string) \mb_substr($string, -(int) \mb_strlen($separator));
			if($tail !== $separator)
			{
				break;
			}
			
			$string = (string) \mb_substr($string, 0, -(int) \mb_strlen($separator));
		}
		
		while((int) \mb_strlen($string) > (int) \mb_strlen($separator))
		{
			$head = (string) \mb_substr($string, 0, (int) \mb_strlen($separator));
			if($head !== $separator)
			{
				break;
			}
			
			$string = (string) \mb_substr($string, (int) \mb_strlen($separator));
		}
		
		return $string;
	}
	
}
