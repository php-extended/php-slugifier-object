<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-slugifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Slugifier;

/**
 * NoopSlugifier class file.
 * 
 * This slugifier is a slugifier that does not transform the entry string.
 * This class is provided as a convenience as a null object for implementations
 * of the namesake design pattern.
 * 
 * @author Anastaszor
 */
class NoopSlugifier implements SlugifierInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Slugifier\SlugifierInterface::isServiceable()
	 */
	public function isServiceable() : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Slugifier\SlugifierInterface::slugify()
	 */
	public function slugify(?string $string, ?SlugifierOptionsInterface $options = null) : string
	{
		return (string) $string;
	}
	
}
