<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-slugifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Slugifier;

/**
 * UppercaseSlugifier class file.
 * 
 * This slugifier transforms the strings to uppercase.
 * 
 * @author Anastaszor
 */
class UppercaseSlugifier implements SlugifierInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Slugifier\SlugifierInterface::isServiceable()
	 */
	public function isServiceable() : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Slugifier\SlugifierInterface::slugify()
	 */
	public function slugify(?string $string, ?SlugifierOptionsInterface $options = null) : string
	{
		return (string) \mb_strtoupper((string) $string);
	}
	
}
