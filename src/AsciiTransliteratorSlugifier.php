<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-slugifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Slugifier;

/**
 * AsciiTransliteratorSlugifier class file.
 * 
 * This slugifier transliterates the given utf8 characters to ascii characters
 * based on the data tables.
 * 
 * @author Anastaszor
 */
class AsciiTransliteratorSlugifier implements SlugifierInterface
{
	
	/**
	 * The options for each string.
	 * 
	 * @var SlugifierOptionsInterface
	 */
	protected SlugifierOptionsInterface $_defaultOptions;
	
	/**
	 * The data tables cache.
	 * 
	 * @var array<integer, array<integer, string>>
	 */
	protected array $_cache = [];
	
	/**
	 * Builds a new AsciiTransliteratorSlugifier with the given default options.
	 * 
	 * @param SlugifierOptionsInterface $defaultOptions
	 */
	public function __construct(?SlugifierOptionsInterface $defaultOptions = null)
	{
		if(null === $defaultOptions)
		{
			$defaultOptions = new SlugifierOptions();
		}
		
		$this->_defaultOptions = $defaultOptions;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Slugifier\SlugifierInterface::isServiceable()
	 */
	public function isServiceable() : bool
	{
		return \extension_loaded('mbstring');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Slugifier\SlugifierInterface::slugify()
	 */
	public function slugify(?string $string, ?SlugifierOptionsInterface $options = null) : string
	{
		$options = (null === $options ? $this->_defaultOptions : $this->_defaultOptions->mergeWith($options));
		
		$oldMbSCh = (string) \ini_get('mbstring.substitute_character');
		\ini_set('mbstring.substitute_character', '');
		
		$utf32 = (string) \mb_convert_encoding((string) $string, 'UTF-32BE', 'UTF-8');
		
		\ini_set('mbstring.substitute_character', $oldMbSCh);
		
		$text = (array) \unpack('N*', $utf32);
		
		$output = '';
		$separated = false;
		
		/** @var integer $code */
		foreach($text as $code)
		{
			$char = $this->getCharFromCodePoint($code);
			if(null === $char || '' === $char)
			{
				if(!$separated)
				{
					$separated = true;
					$output .= $options->getSeparator();
				}
				continue;
			}
			
			$output .= $char;
			$separated = false;
		}
		
		
		return \trim($output, $options->getSeparator());
	}
	
	/**
	 * Gets the char from the code point.
	 * 
	 * @param integer $code
	 * @return ?string
	 */
	public function getCharFromCodePoint(int $code) : ?string
	{
		if(0x1D7FF < $code) // not in the tables
		{
			return null;
		}
		
		if(0x80 > $code) // ascii, let it be
		{
			return \chr($code);
		}
		
		$section = $code >> 8;
		$position = $code & 0xFF;
		
		if(!isset($this->_cache[$section]))
		{
			$filename = \dirname(__DIR__).\DIRECTORY_SEPARATOR.'data'.\DIRECTORY_SEPARATOR.'x'.((string) \sprintf('%03x', $section)).'.php';
			$this->_cache[$section] = [];
			if(\file_exists($filename))
			{
				/** @psalm-suppress MixedPropertyTypeCoercion */
				$this->_cache[$section] = require $filename;
			}
		}
		
		/** @var array<integer, string> $cachedSection */
		$cachedSection = $this->_cache[$section] ?? [];
		if([] === $cachedSection)
		{
			return null;
		}
		
		/** @var ?string $cachedChar */
		$cachedChar = $cachedSection[$position] ?? null;
		if(null === $cachedChar)
		{
			return null;
		}
		
		return (string) $cachedChar;
	}
	
}
