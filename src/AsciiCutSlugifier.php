<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-slugifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Slugifier;

/**
 * AsciiCutSlugifier class file.
 * 
 * This slugifier removes all letter nor digits characters and replaces them
 * with the replacement string.
 * 
 * @author Anastaszor
 */
class AsciiCutSlugifier implements SlugifierInterface
{
	
	/**
	 * The default options.
	 * 
	 * @var ?SlugifierOptionsInterface
	 */
	protected ?SlugifierOptionsInterface $_defaultOptions;
	
	/**
	 * Builds the martinml bridge with default options for the slugifier.
	 * 
	 * @param ?SlugifierOptionsInterface $default
	 */
	public function __construct(?SlugifierOptionsInterface $default = null)
	{
		$this->_defaultOptions = $default;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Slugifier\SlugifierInterface::isServiceable()
	 */
	public function isServiceable() : bool
	{
		return \extension_loaded('pcre');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Slugifier\SlugifierInterface::slugify()
	 */
	public function slugify(?string $string, ?SlugifierOptionsInterface $options = null) : string
	{
		$fullOptions = null;
		
		if(null !== $this->_defaultOptions && null !== $options)
		{
			$fullOptions = $this->_defaultOptions->mergeWith($options);
		}
		
		if(null === $fullOptions && null !== $options)
		{
			$fullOptions = $options;
		}
		
		if(null === $fullOptions)
		{
			$fullOptions = $this->_defaultOptions;
		}
		
		$separator = '-';
		if(null !== $fullOptions)
		{
			$separator = $fullOptions->getSeparator();
		}
		
		return (string) \preg_replace('#[^\\w]+#', $separator, (string) $string);
	}
	
}
