<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-slugifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Slugifier;

/**
 * SlugifierOptions class file.
 * 
 * This class specifies the options applicable to the slugifiers.
 * 
 * @author Anastaszor
 */
class SlugifierOptions implements SlugifierOptionsInterface
{
	
	/**
	 * The separator for slugified strings.
	 * 
	 * @var string
	 */
	protected string $_separator = '-';
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the separator for slugified strings.
	 * 
	 * @param ?string $separator
	 * @return SlugifierOptions
	 */
	public function setSeparator(?string $separator) : SlugifierOptions
	{
		$this->_separator = (string) $separator;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Slugifier\SlugifierOptionsInterface::getSeparator()
	 */
	public function getSeparator() : string
	{
		return $this->_separator;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Slugifier\SlugifierOptionsInterface::mergeWith()
	 */
	public function mergeWith(?SlugifierOptionsInterface $other) : SlugifierOptionsInterface
	{
		$new = new self();
		
		if(null !== $other)
		{
			$new->setSeparator($other->getSeparator());
		}
		
		return $new;
	}
	
}
