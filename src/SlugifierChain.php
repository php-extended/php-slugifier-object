<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-slugifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Slugifier;

/**
 * SlugifierChain class file.
 * 
 * This class is to chain different slugifiers to have a more slugified string.
 * It is to be noted that the order of the slugifier matters, and the resulting
 * strings may be different for the same input if the order is not preserved.
 * 
 * @author Anastaszor
 */
class SlugifierChain implements SlugifierInterface
{
	
	/**
	 * The chain of slugifiers, in order.
	 * 
	 * @var array<integer, SlugifierInterface>
	 */
	protected array $_slugifiers = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds a slugifier to the end of the chain.
	 * 
	 * @param SlugifierInterface $slugifier
	 */
	public function addSlugifier(SlugifierInterface $slugifier) : void
	{
		$this->_slugifiers[] = $slugifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Slugifier\SlugifierInterface::isServiceable()
	 */
	public function isServiceable() : bool
	{
		foreach($this->_slugifiers as $slugifier)
		{
			if(!$slugifier->isServiceable())
			{
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Slugifier\SlugifierInterface::slugify()
	 */
	public function slugify(?string $string, ?SlugifierOptionsInterface $options = null) : string
	{
		$workstring = $string;
		
		foreach($this->_slugifiers as $slugifier)
		{
			$workstring = $slugifier->slugify($workstring, $options);
		}
		
		return (string) $workstring;
	}
	
}
