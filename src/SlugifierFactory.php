<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-slugifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Slugifier;

/**
 * SlugifierFactory class file.
 * 
 * This class is a simple implementation of the SlugifierFactoryInterface.
 * 
 * @author Anastaszor
 */
class SlugifierFactory implements SlugifierFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Slugifier\SlugifierFactoryInterface::createSlugifier()
	 */
	public function createSlugifier(?SlugifierOptionsInterface $slugifierOptions = null) : SlugifierInterface
	{
		$slugifier = new SlugifierChain();
		$slugifier->addSlugifier(new AsciiTransliteratorSlugifier());
		$slugifier->addSlugifier(new TrimSlugifier($slugifierOptions));
		$slugifier->addSlugifier(new LowercaseSlugifier());
		$slugifier->addSlugifier(new AsciiCutSlugifier($slugifierOptions));
		
		return $slugifier;
	}
	
}
