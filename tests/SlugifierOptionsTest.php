<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-slugifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Slugifier\SlugifierOptions;
use PHPUnit\Framework\TestCase;

/**
 * SlugifierOptionsTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Slugifier\SlugifierOptions
 *
 * @internal
 *
 * @small
 */
class SlugifierOptionsTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var SlugifierOptions
	 */
	protected SlugifierOptions $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testSeparatorNull() : void
	{
		$this->_object->setSeparator(null);
		$this->assertEquals('', $this->_object->getSeparator());
	}
	
	public function testSeparatorSimple() : void
	{
		$this->_object->setSeparator('-');
		$this->assertEquals('-', $this->_object->getSeparator());
	}
	
	public function testSeparatorComplex() : void
	{
		$this->_object->setSeparator('&&');
		$this->assertEquals('&&', $this->_object->getSeparator());
	}
	
	public function testMerge() : void
	{
		$new = new SlugifierOptions();
		$new->setSeparator('|');
		$this->assertEquals('|', $this->_object->mergeWith($new)->getSeparator());
		$this->assertEquals('-', $new->mergeWith($this->_object)->getSeparator());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new SlugifierOptions();
	}
	
}
