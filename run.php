<?php declare(strict_types=1);

/**
 * Test script for AsciiTransliteratorSlugifier.
 * 
 * Usage:
 * php test.php "<data>"
 * 
 * @author Anastaszor
 */

use PhpExtended\Slugifier\AsciiTransliteratorSlugifier;

global $argv;

if(!isset($argv[1]))
	throw new InvalidArgumentException('The first argument should be the string to slugify.');
$data = $argv[1];

echo "\n\t".'Input : '.$data."\n";

$composer = __DIR__.'/vendor/autoload.php';
if(!is_file($composer))
	throw new RuntimeException('You should run composer first.');
require $composer;

$slugifier = new AsciiTransliteratorSlugifier();

echo "\t".'Slug : '.$slugifier->slugify($data)."\n\n";
